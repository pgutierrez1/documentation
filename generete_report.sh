#!/usr/bin/env bash
FORMAT=$1
mkdir -p target/$FORMAT
java -jar openapi-generator-cli-3.3.4.jar generate -i ./swagger/datahub-openapi-3.0.0.json -l $FORMAT -o target/$FORMAT